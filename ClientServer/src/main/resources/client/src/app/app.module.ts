import { JobService } from './core/job.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { PostjobComponent } from './postjob/postjob.component';
import { HeaderComponent } from './header/header.component';
import { FindjobComponent } from './findjob/findjob.component';
import { EditjobComponent } from './editjob/editjob.component';
import {routing} from "./app.routes";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { ViewjobComponent } from './viewjob/viewjob.component';
import { SplitSentence } from './split-sentence';
import { LoginComponent } from './login/login.component';
import {LoginService} from "./core/login.service";

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    PostjobComponent,
    HeaderComponent,
    FindjobComponent,
    EditjobComponent,
    ViewjobComponent,
    SplitSentence,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    routing,
  ],
  providers: [JobService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
