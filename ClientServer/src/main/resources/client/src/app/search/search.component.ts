import { JobService } from './../core/job.service';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Job } from './../job';
import { Component, OnInit } from '@angular/core';
import {LoginService} from "../core/login.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  jobs: Job[] = []; //= [new Job('', [new JopType(null, '')], '', '', '')];
  errorMessage: string;
  private alive: boolean = true;

  successMessage: string;
  alertType: string;

  searchForm: FormGroup;
  constructor(private fb: FormBuilder, private jobService: JobService, private loginService: LoginService) {
    this.createForm();
  }

  ngOnInit() {
    this.loginService.checkCredentials();
  }
  createForm() {
    this.searchForm = this.fb.group({
      'keyword': ['', Validators.required],
      'location': ['', Validators.required]
    });
  }

  onSearch() {
    this.jobService.findAllJob()//searchJobs(this.searchForm.value)
      .takeWhile(() => this.alive)
      .subscribe(
        (jobs: Job[]) => {
          this.jobs = jobs;
        },
        error => this.toast("No data found!", "danger")
      );
    // if (this.jobs) {
    //   //  Populate data
    // }
  }

  public ngOnDestroy() {
    // console.log(this.jobs);
    this.alive = false;
  }
  toast(text: any, alertType: string = "info") {
    this.successMessage = text;
    this.alertType = alertType;
  }

}
