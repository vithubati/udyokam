import { Location } from './location';
export class Employer {
    constructor(public id: number, public companyName: string, public location: Location, 
                public email: string, public website: string, public phoneOne: string, 
                public phoneTwo: string) {}
}
