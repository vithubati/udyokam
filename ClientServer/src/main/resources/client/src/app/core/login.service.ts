import { Injectable } from '@angular/core';
import {Router} from "@angular/router";
import { Cookie } from 'ng2-cookies';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {Job} from "../job";
import {log} from "util";

@Injectable()
export class LoginService {

  constructor(private _router: Router, private _http: Http) { }


  obtainAccessToken(loginData){
    let params = new URLSearchParams();
    params.append('username',loginData.username);
    params.append('password',loginData.password);
    params.append('grant_type','password');
    params.append('client_id','fooClientIdPassword');

    let headers = new Headers({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic '+btoa("fooClientIdPassword:secret")});
    let options = new RequestOptions({ headers: headers });
    console.log(params.toString());
    this._http.post('http://localhost:8081/AuthorizationServer/oauth/token', params.toString(), options)
      .map(res => res.json())
      .subscribe(
        data => this.saveTokenInLocal(data),
        err => alert('Invalid Credentials')
      );
  }

  saveTokenInCookie(token){
    console.log(token);
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token, expireDate);
    console.log(Cookie.get("access_token"));
    this._router.navigate(['/']);
  }

  saveTokenInLocal(token){
    console.log(token);
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    localStorage.setItem('access_token', token.access_token);
    localStorage.setItem('expires_in', expireDate.toString());
    console.log(localStorage);
    console.log('Obtained Access token');
    this._router.navigate(['/']);
  }

  getResource(resourceUrl) : Observable<Job>{
    var headers = new Headers({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer '+localStorage.get('access_token')
    });
    var options = new RequestOptions({ headers: headers });
    return this._http.get(resourceUrl, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  checkCredentials(){
    if (!localStorage.getItem('access_token')){
      this._router.navigate(['/login']);
    }
  }

  logout() {
    localStorage.removeItem('access_token');
    this._router.navigate(['/login']);
  }

}
