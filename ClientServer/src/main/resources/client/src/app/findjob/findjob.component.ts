import { JobService } from './../core/job.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {Job} from "app/job";
import {LoginService} from "../core/login.service";

@Component({
  selector: 'app-findjob',
  templateUrl: './findjob.component.html',
  styleUrls: ['./findjob.component.css']
})
export class FindjobComponent implements OnInit, OnDestroy {
  // @Input()
  jobs: Job[] = []; //= [new Job('', [new JopType(null, '')], '', '', '')];
  errorMessage: string;
  mode = 'Observable';
  private alive: boolean = true;

  successMessage: string;
  alertType: string;
  findForm: FormGroup;
  constructor(private fb: FormBuilder, private jobService: JobService, private loginService: LoginService) {
    this.createForm();
  }

  ngOnInit() {
    this.loginService.checkCredentials();
    this.onFind();
  }

  createForm() {
    this.findForm = this.fb.group({
      'keyword': [''],
      'location': ['']
    });
  }

  onFind() {
    this.jobService.searchJobs(this.findForm.value)
      .takeWhile(() => this.alive)
      .subscribe(
        (jobs: Job[]) => {
          this.handleResponse(jobs);
        },
        error => this.toast("No data found!", "danger")
      );
  }

  public ngOnDestroy() {
    this.alive = false;
  }
  handleResponse(jobs){
    if(jobs.hasOwnProperty('errorCode')){
      this.toast(jobs.message);
    }else if(jobs.length==0){
      this.jobs = [];
      this.toast("No Jobs found!");
    }else{
      this.successMessage = null;
      this.jobs = jobs;
    }
  }
  toast(text: any, alertType: string = "info") {
    this.successMessage = text;
    this.alertType = alertType;
  }
}
