package com.dharmalion.udyokam.controller;

import com.dharmalion.udyokam.dao.IJobDAO;
import com.dharmalion.udyokam.model.ErrorResponse;
import com.dharmalion.udyokam.model.Job;
import com.dharmalion.udyokam.service.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import com.dharmalion.udyokam.exception.JobNotFoundException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( value = "/api/job" )
public class JobController {

    private static final Logger logger = LoggerFactory
            .getLogger(JobController.class);

    @Autowired
    private JobService jobService;

    @PreAuthorize("#oauth2.hasScope('foo') and #oauth2.hasScope('read')")
    @RequestMapping( method = RequestMethod.GET )
    public List<Job> getAllJobs(){
        System.out.println(" Vithu" + jobService.getAllJobs());
        return jobService.getAllJobs();
    }

    @RequestMapping( method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity getJobById(@PathVariable int id) {
        Job job = jobService.getJobById(id);

        if (job == null)
            throw new JobNotFoundException(id);

        return new ResponseEntity<Job>(job, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Job postJob(@RequestBody Job job) {
        System.out.println("Job details" + job);
        return jobService.postJob(job);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public Job updateJob(@RequestBody Job job, @PathVariable int id){
        return jobService.updateJob(job, id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteJob( @PathVariable int id){
        jobService.deleteJob(id);
    }

    /**
     * handle only the JobNotFoundException thrown in any of the layers of the application
     * @param ex
     * @return
     */
    @ExceptionHandler(JobNotFoundException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.BAD_REQUEST.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

    /**
     * with Spring Data JPA Specifications
     * @param query
     * @return
     */
    @PreAuthorize("#oauth2.hasScope('read')")
    @RequestMapping( method = RequestMethod.GET, value = "/fetch" )
    public List<Job> getSearchedJobs(@RequestParam(value = "query", required = false) String query) {
        return this.jobService.getSearchedJobs(query);
    }

}
