package com.dharmalion.udyokam.model;

import javax.persistence.*;

/**
 * Created by bati on 6/10/17.
 */
@Entity
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String addressOne;
    private String addressTwo;
    private String city;
    private String province;
    private String postalCode;

    @ManyToOne
    @JoinColumn(name = "country")
    private Country country;

    public Location() {
    }

    public Location(String addressOne, String addressTwo, String city, String province, String postalCode) {
        this.addressOne = addressOne;
        this.addressTwo = addressTwo;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
    }

    public Location(String addressOne, String addressTwo, String city, String province, String postalCode, Country country) {
        this.addressOne = addressOne;
        this.addressTwo = addressTwo;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public String getAddressTwo() {
        return addressTwo;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
