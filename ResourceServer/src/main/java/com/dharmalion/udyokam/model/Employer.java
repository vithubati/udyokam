package com.dharmalion.udyokam.model;

import javax.persistence.*;

/**
 * Created by bati on 6/10/17.
 */
@Entity
public class Employer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String companyName;
    private String website;

    @ManyToOne
    @JoinColumn(name = "location")
    private Location location;

    private String email;
    private String phoneOne;
    private String phoneTwo;

    public Employer() {
    }

    public Employer(String companyName, Location location, String website, String email, String phoneOne, String phoneTwo) {
        this.companyName = companyName;
        this.location = location;
        this.website = website;
        this.email = email;
        this.phoneOne = phoneOne;
        this.phoneTwo = phoneTwo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneOne() {
        return phoneOne;
    }

    public void setPhoneOne(String phoneOne) {
        this.phoneOne = phoneOne;
    }

    public String getPhoneTwo() {
        return phoneTwo;
    }

    public void setPhoneTwo(String phoneTwo) {
        this.phoneTwo = phoneTwo;
    }
}
