package com.dharmalion.udyokam.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by bati on 5/14/17.
 */
@Entity
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String title;

    @ManyToOne
    @JoinColumn(name = "job_type")
    private JobType jobType;

    private String company;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "location")
    private Location location;

    private int resumeRequired;

    private String salary;

    private String description;

    @ManyToOne
    @JoinColumn(name = "education")
    private Education education;

    @ManyToOne
    @JoinColumn(name = "experience")
    private Experience experience;

    @ManyToOne
    @JoinColumn(name = "category")
    private JobCategory category;

    @ManyToOne
    @JoinColumn(name = "industry")
    private Industry industry;

    private String requirements;

    private String responsibilities;

    private String employerSite;

    @ManyToOne
    @JoinColumn(name = "employer")
    private Employer employer;

    private int active;

    private int deleted;

    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;

    private double workHours;

    public Job() {
    }

    public Job(String title, JobType jobType, String company, Date createdDate, Location location, int resumeRequired, String salary, String description, Education education, Experience experience, JobCategory category, Industry industry, String requirements, String responsibilities, String employerSite, Employer employer, int active, int deleted, Date deadline, double workHours) {
        this.title = title;
        this.jobType = jobType;
        this.company = company;
        this.createdDate = createdDate;
        this.industry = industry;
        this.location = location;
        this.resumeRequired = resumeRequired;
        this.salary = salary;
        this.description = description;
        this.education = education;
        this.experience = experience;
        this.category = category;
        this.requirements = requirements;
        this.responsibilities = responsibilities;
        this.employerSite = employerSite;
        this.employer = employer;
        this.active = active;
        this.deleted = deleted;
        this.deadline = deadline;
        this.workHours = workHours;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public JobType getJobType() {
        return jobType;
    }

    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getResumeRequired() {
        return resumeRequired;
    }

    public void setResumeRequired(int resumeRequired) {
        this.resumeRequired = resumeRequired;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public Experience getExperience() {
        return experience;
    }

    public void setExperience(Experience experience) {
        this.experience = experience;
    }

    public JobCategory getCategory() {
        return category;
    }

    public void setCategory(JobCategory category) {
        this.category = category;
    }

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getEmployerSite() {
        return employerSite;
    }

    public void setEmployerSite(String employerSite) {
        this.employerSite = employerSite;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public double getWorkHours() {
        return workHours;
    }

    public void setWorkHours(double workHours) {
        this.workHours = workHours;
    }
}
