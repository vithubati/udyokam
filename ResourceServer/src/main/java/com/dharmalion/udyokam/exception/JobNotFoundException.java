package com.dharmalion.udyokam.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="No Job found")  // 404
public class JobNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -3332292346834265371L;

    public JobNotFoundException(int id){
        super("No Job found for id: "+id);
    }

    public JobNotFoundException(){
        super("No Job found!");
    }
}
