package com.dharmalion.udyokam.exception;

/**
 * Created by bati on 6/11/17.
 */
public class JobException  extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }
    public JobException(String errorMessage) {
        super(errorMessage);
        this.errorMessage = errorMessage;
    }
    public JobException() {
        super();
    }
}
