package com.dharmalion.udyokam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by bati on 5/13/17.
 */
@SpringBootApplication
public class ApplicationController extends SpringBootServletInitializer {

    public static void main(String[] args){
        SpringApplication.run(ApplicationController.class, args);
    }
}
