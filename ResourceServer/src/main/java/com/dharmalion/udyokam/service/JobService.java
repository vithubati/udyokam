package com.dharmalion.udyokam.service;

import com.dharmalion.udyokam.dao.JobRepository;
import com.dharmalion.udyokam.dao.JobSpecificationsBuilder;
import com.dharmalion.udyokam.dao.LocationRepository;
import com.dharmalion.udyokam.exception.JobNotFoundException;
import com.dharmalion.udyokam.model.Job;
import com.dharmalion.udyokam.model.Location;

import com.dharmalion.udyokam.util.SearchOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional
public class JobService {

    @Autowired
    private final JobRepository jobRepository;

    @Autowired
    private final LocationRepository locationRepository;

    public JobService(JobRepository jobRepository, LocationRepository locationRepository) {
        this.jobRepository = jobRepository;
        this.locationRepository = locationRepository;
    }

    public List<Job> getAllJobs(){
        List<Job> jobs = new ArrayList<Job>();
        for (Job job : jobRepository.findAll()) {
            jobs.add(job);
        }
        return jobs;
    }

    public Job getJobById(int id){
        List<Job> jobs = new ArrayList<Job>();
        return jobRepository.findOne(id);
    }

    public Job postJob(Job job) {
        Location loc = job.getLocation();
        locationRepository.save(loc);
        return jobRepository.save(job);
    }

    public Job updateJob(Job job, int id){
        job.setId(id);
        return jobRepository.save(job);
    }

    /**
     * Fetch quarried Job details
     * @param query
     * @return List<Job>
     */
    public List<Job> getSearchedJobs(String query){
        JobSpecificationsBuilder builder = new JobSpecificationsBuilder();
        String operationSetExper = Joiner.on("|").join(SearchOperation.SIMPLE_OPERATION_SET);

        Pattern pattern = Pattern.compile("(\\w+?)(" + operationSetExper + ")(\\p{Punct}?)(\\w+?)(\\p{Punct}?),");
        System.out.println("query== " +query);
        Matcher matcher = pattern.matcher(query + ",");
        System.out.println("matcher== " +matcher);
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(4), matcher.group(3), matcher.group(5));
        }

        Specification<Job> spec = builder.build();
        System.out.println("Spec = "+ spec.toString());
        if(spec == null)
            throw new JobNotFoundException();
        return jobRepository.findAll(spec);
    }

    public void deleteJob(int id){
        jobRepository.delete(id);
    }
}
