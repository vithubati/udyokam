package com.dharmalion.udyokam.util;

/**
 * Specifications technique Search
 */
public class SearchCriteria {
    private String key;
    private SearchOperation operation;
    private Object value;
    private boolean orPredicate;

    public SearchCriteria() {

    }

    /**
     *
     * @param orPredicate
     * @param key Used to hold field name – for example: firstName, age, … etc.
     * @param operation Used to hold the operation – for example: Equality, less than, … etc.
     * @param value Used to hold the field value – for example: john, 25, … etc.
     */
    public SearchCriteria(final String orPredicate, final String key, final SearchOperation operation, final Object value) {
        super();
        this.orPredicate = orPredicate != null && orPredicate.equals(SearchOperation.OR_PREDICATE_FLAG);
        this.key = key;
        this.operation = operation;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public SearchOperation getOperation() {
        return operation;
    }

    public void setOperation(final SearchOperation operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(final Object value) {
        this.value = value;
    }

    public boolean isOrPredicate() {
        return orPredicate;
    }
    public void setOrPredicate(boolean orPredicate) {
        this.orPredicate = orPredicate;
    }
}
