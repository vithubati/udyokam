package com.dharmalion.udyokam.dao;

import com.dharmalion.udyokam.model.Job;
import com.dharmalion.udyokam.util.SearchCriteria;
import com.dharmalion.udyokam.util.SearchOperation;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.List;

public class JobSpecificationsBuilder {
    private final List<SearchCriteria> criteria;

    public JobSpecificationsBuilder() {
        criteria = new ArrayList<>();
    }

    public final JobSpecificationsBuilder with(final String key, final String operation, final Object value,
                                               final String prefix, final String suffix) {
        System.out.println("Key= " + key + " Operation= " + operation + " value= " + value + " prefix= " + prefix + " suffix= "+ suffix);
        return with(null, key, operation, value, prefix, suffix);
    }

    public final JobSpecificationsBuilder with(final String orPredicate, final String key, final String operation,
                                               final Object value, final String prefix, final String suffix) {
        SearchOperation op = SearchOperation.getSimpleOperation(operation.charAt(0));
        if (op != null) {
            if (op == SearchOperation.EQUALITY) { // the operation may be complex operation
                final boolean startWithAsterisk = prefix != null && prefix.contains(SearchOperation.ZERO_OR_MORE_REGEX);
                final boolean endWithAsterisk = suffix != null && suffix.contains(SearchOperation.ZERO_OR_MORE_REGEX);

                if (startWithAsterisk && endWithAsterisk) {
                    op = SearchOperation.CONTAINS;
                } else if (startWithAsterisk) {
                    op = SearchOperation.ENDS_WITH;
                } else if (endWithAsterisk) {
                    op = SearchOperation.STARTS_WITH;
                }
            }
            System.out.println("orPredicate= " + orPredicate + " key= " + key + " op= " + op + " value= "+ value);
            criteria.add(new SearchCriteria(orPredicate, key, op, value));
        }
        System.out.println(this);
        return this;
    }

    public Specification<Job> build() {
        if (criteria.size() == 0)
            return null;

        Specification<Job> result = new JobSpecification(criteria.get(0));
        for (int i = 1; i < criteria.size(); i++) {
            System.out.println("criteria" + criteria.get(i));
            result = criteria.get(i).isOrPredicate()
                    ? Specifications.where(result).or(new JobSpecification(criteria.get(i)))
                    : Specifications.where(result).and(new JobSpecification(criteria.get(i)));
        }
        System.out.println("Result = "+ result.toString());
        return result;
    }
}
