package com.dharmalion.udyokam.dao;

import com.dharmalion.udyokam.model.Job;
import com.dharmalion.udyokam.util.SearchCriteria;

import java.util.List;



public interface IJobDAO {
    List<Job> searchJob(List<SearchCriteria> params);

    void save(Job entity);
}
