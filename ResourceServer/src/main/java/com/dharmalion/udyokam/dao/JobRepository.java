package com.dharmalion.udyokam.dao;

import com.dharmalion.udyokam.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface JobRepository extends JpaRepository<Job, Integer>, JpaSpecificationExecutor<Job> {
    List findByCompany(String company);
    List findByLocation_City(String city);

}
