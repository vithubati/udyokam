package com.dharmalion.udyokam.dao;

import com.dharmalion.udyokam.model.Location;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by bati on 5/14/17.
 */
public interface LocationRepository extends CrudRepository<Location, Integer> {

}
