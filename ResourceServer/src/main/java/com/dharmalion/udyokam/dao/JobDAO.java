package com.dharmalion.udyokam.dao;

import com.dharmalion.udyokam.model.Job;
import com.dharmalion.udyokam.util.SearchCriteria;
import com.dharmalion.udyokam.util.SearchOperation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class JobDAO implements IJobDAO{

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Deprecated method of search operation
     * @param criterias
     * @return List<Job>
     */
    @Override
    public List<Job> searchJob(List<SearchCriteria> criterias) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Job> query = builder.createQuery(Job.class);
        Root r = query.from(Job.class);

        Predicate predicate = builder.conjunction();

        for (SearchCriteria criteria : criterias) {
            if (criteria.getOperation() == SearchOperation.GREATER_THAN) {
                predicate = builder.and(predicate,
                        builder.greaterThanOrEqualTo(r.get(criteria.getKey()),
                                criteria.getValue().toString()));
            } else if (criteria.getOperation() == SearchOperation.LESS_THAN) {
                predicate = builder.and(predicate,
                        builder.lessThanOrEqualTo(r.get(criteria.getKey()),
                                criteria.getValue().toString()));
            } else if (criteria.getOperation() == SearchOperation.EQUALITY) {
                if (r.get(criteria.getKey()).getJavaType() == String.class) {
                    predicate = builder.and(predicate,
                            builder.like(r.get(criteria.getKey()),
                                    "%" + criteria.getValue() + "%"));
                } else {
                    predicate = builder.and(predicate,
                            builder.equal(r.get(criteria.getKey()), criteria.getValue()));
                }
            }
        }
        query.where(predicate);

        List<Job> result = entityManager.createQuery(query).getResultList();
        return result;
    }

    @Override
    public void save(Job entity) {
        entityManager.persist(entity);
    }
}
